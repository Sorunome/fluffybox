import 'dart:convert';
import 'package:crypto/crypto.dart';

import 'package:hive/hive.dart' as h show Box;
import 'package:hive/hive.dart' hide Box;

class BoxCollection {
  final String name;
  final Set<String> boxNames;
  HiveCipher? _cipher;

  BoxCollection(this.name, this.boxNames);

  static bool _hiveInit = false;

  late h.Box<String> _badKeyBox;

  static Future<BoxCollection> open(
    String name,
    Set<String> boxNames, {
    String? path,
    HiveCipher? key,
  }) async {
    if (!_hiveInit) {
      Hive.init(path ?? './');
      _hiveInit = true;
    }
    final collection = BoxCollection(name, boxNames);
    if (key != null) {
      collection._cipher = key;
    }
    collection._badKeyBox = await Hive.openBox<String>('${name}_bad_keys');

    return collection;
  }

  Future<Box<V>> openBox<V>(String name, {bool preload = false}) async {
    if (!boxNames.contains(name)) {
      throw ('Box with name $name is not in the known box names of this collection.');
    }
    final i = _openBoxes.indexWhere((box) => box.name == name);
    if (i != -1) {
      return _openBoxes[i] as Box<V>;
    }
    final box = Box<V>('${this.name}_$name', this);
    if (preload) {
      box._cachedBox = await Hive.openBox(box.name, encryptionCipher: _cipher);
    }
    _openBoxes.add(box);
    return box;
  }

  final List<Box> _openBoxes = [];

  Future<void> transaction(
    Future<void> Function() action, {
    List<String>? boxNames,
    bool readOnly = false,
  }) =>
      action();

  void close() {
    for (final box in _openBoxes) {
      box._cachedBox?.close();
    }
  }

  Future<void> deleteFromDisk() => Future.wait(
        boxNames.map(Hive.deleteBoxFromDisk),
      );
}

class Box<V> {
  final String name;
  final BoxCollection boxCollection;

  BoxBase? _cachedBox;

  Future<BoxBase> _getBox() async {
    return _cachedBox ??= await Hive.openLazyBox<V>(
      name,
      encryptionCipher: boxCollection._cipher,
    );
  }

  Box(this.name, this.boxCollection) {
    if (!(V is String ||
        V is bool ||
        V is int ||
        V is Object ||
        V is List<Object?> ||
        V is Map<String, Object?> ||
        V is double)) {
      throw ('Value type ${V.runtimeType} is not one of the allowed value types {String, int, double, List<Object?>, Map<String, Object?>}.');
    }
  }

  Future<List<String>> getAllKeys() async {
    final box = await _getBox();
    return box.keys
        .cast<String>()
        .map((key) {
          if (key.startsWith(_badKeyPrefix)) {
            key = boxCollection._badKeyBox.get(key) ?? key;
          }
          return key;
        })
        .map(Uri.decodeComponent)
        .toList();
  }

  Future<Map<String, V>> getAllValues() async {
    final box = await _getBox();
    final keys = box.keys.toList();
    if (box is LazyBox) {
      final values = await Future.wait(keys.map(box.get));
      return {
        for (var i = 0; i < values.length; i++)
          Uri.decodeComponent(keys[i]): values[i]
      };
    }
    return (box as h.Box)
        .toMap()
        .map((k, v) => MapEntry(Uri.decodeComponent(k), v));
  }

  Future<V?> get(String key) async {
    key = _toHiveKey(key);
    final box = await _getBox();
    if (box is LazyBox) return await box.get(key);
    return (box as h.Box).get(key);
  }

  Future<List<V?>> getAll(
    List<String> keys,
  ) async {
    final box = await _getBox();
    final values = <V?>[];
    for (var key in keys) {
      key = _toHiveKey(key);
      if (box is LazyBox) {
        values.add(await box.get(key));
      } else {
        values.add((box as h.Box).get(key));
      }
    }
    return values;
  }

  Future<void> put(String key, V val) async {
    if (val == null) {
      return delete(key);
    }
    final box = await _getBox();
    await box.put(_toHiveKey(key), val);
    return;
  }

  Future<void> delete(String key) async {
    final box = await _getBox();
    await box.delete(_toHiveKey(key));
    return;
  }

  Future<void> deleteAll(List<String> keys) async {
    final hiveKeys = keys.map(_toHiveKey);
    final box = await _getBox();
    await box.deleteAll(hiveKeys);
    return;
  }

  Future<void> clear() async {
    final box = await _getBox();
    await box.deleteAll(box.keys);
    return;
  }

  static const int _maxKeyLength = 255;
  static const String _badKeyPrefix = '_bad_key_';

  String _calcHashKey(String encodedKey) =>
      _badKeyPrefix + sha256.convert(utf8.encode(encodedKey)).toString();

  String _toHiveKey(String key) {
    final encodedKey = key.split('|').map(Uri.encodeComponent).join('|');
    if (encodedKey.length >= _maxKeyLength) {
      final hashKey = _calcHashKey(encodedKey);
      boxCollection._badKeyBox.put(hashKey, encodedKey);
      return hashKey;
    }
    return encodedKey;
  }
}
