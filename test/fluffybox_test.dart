import 'dart:io';
import 'dart:math';

import 'package:file/memory.dart';
import 'package:test/test.dart';

import 'package:fluffybox/fluffybox.dart';

void main() {
  group('FluffyBox tests', () {
    late BoxCollection collection;
    const Set<String> boxNames = {'cats', 'numbers', 'longkeys'};
    const data = {'name': 'Fluffy', 'age': 2};
    const data2 = {'name': 'Loki', 'age': 4};
    final fileSystem = MemoryFileSystem();
    final testHivePath =
        '${fileSystem.path}/build/.test_store/${Random().nextDouble()}';
    Directory(testHivePath).createSync(recursive: true);
    setUp(() async {
      collection = await BoxCollection.open(
        'testbox',
        boxNames,
        path: testHivePath,
      );
    });

    test('Box.put and Box.get', () async {
      final box = await collection.openBox<Map>('cats');
      await box.put('fluffy', data);
      expect(await box.get('fluffy'), data);
      await box.clear();
    });

    test('Box.getAll', () async {
      final box = await collection.openBox<Map>('cats');
      await box.put('fluffy', data);
      await box.put('loki', data2);
      expect(await box.getAll(['fluffy', 'loki']), [data, data2]);
      await box.clear();
    });

    test('Box.getAllKeys', () async {
      final box = await collection.openBox<Map>('cats');
      await box.put('fluffy', data);
      await box.put('loki', data2);
      expect(await box.getAllKeys(), ['fluffy', 'loki']);
      await box.clear();
    });

    test('Box.getAllValues', () async {
      final box = await collection.openBox<Map>('cats');
      await box.put('fluffy', data);
      await box.put('loki', data2);
      expect(await box.getAllValues(), {'fluffy': data, 'loki': data2});
      await box.clear();
    });

    test('Box.delete', () async {
      final box = await collection.openBox<Map>('cats');
      await box.put('fluffy', data);
      await box.put('loki', data2);
      await box.delete('fluffy');
      expect(await box.get('fluffy'), null);
      await box.clear();
    });

    test('Box.deleteAll', () async {
      final box = await collection.openBox<Map>('cats');
      await box.put('fluffy', data);
      await box.put('loki', data2);
      await box.deleteAll(['fluffy', 'loki']);
      expect(await box.get('fluffy'), null);
      expect(await box.get('loki'), null);
      await box.clear();
    });

    test('Box.clear', () async {
      final box = await collection.openBox<Map>('cats');
      await box.put('fluffy', data);
      await box.put('loki', data2);
      await box.clear();
      expect(await box.get('fluffy'), null);
      expect(await box.get('loki'), null);
    });

    test('Box.close', () async {
      var box = await collection.openBox<Map>('cats');
      await box.put('fluffy', data);
      collection.deleteFromDisk();
      collection = await BoxCollection.open(
        'testbox',
        boxNames,
        path: testHivePath,
      );
      box = await collection.openBox<Map>('cats');
      await box.put('fluffy', data);
    });

    test('BoxCollection.transaction', () async {
      final box = await collection.openBox<int>('numbers');
      await collection.transaction(() async {
        await box.put('key', 1);
        await box.put('key', 2);
        await box.put('key', 3);
        await box.put('key', 4);
        await box.put('key', 5);
        await box.put('key', 6);
        await box.put('key', 7);
        await box.put('key', 8);
        await box.put('key', 9);
        await box.put('key', 10);
        expect(await box.get('key'), 10);
        await box.put('key', 11);
        await box.put('key', 12);
        await box.put('key', 13);
        await box.put('key', 14);
        await box.put('key', 15);
        await box.put('key', 16);
        await box.put('key', 17);
        await box.put('key', 18);
        await box.put('key', 19);
        await box.put('key', 20);
      });
      expect(await box.get('key'), 20);
    });

    test('Too long hive key', () async {
      final box = await collection.openBox<int>('longkeys');
      final longKey = List.filled(257, 'a').join('');
      await box.put(longKey, 42);
      expect(await box.get(longKey), 42);

      final longKeySeparated = '\$roomid:example.com|$longKey';
      await box.put(longKeySeparated, 42);
      expect(await box.get(longKeySeparated), 42);

      final keys = await box.getAllKeys();
      expect(keys.toSet(), {longKey, longKeySeparated});
    });
  });
}
